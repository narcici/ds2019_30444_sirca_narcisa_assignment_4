package com.example.consumer;

import com.example.consumer.controllers.LoginController;
import com.example.consumer.views.LoginView;
import com.example.consumer.wsdl.GetLoginResponse;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import com.example.consumer.wsdl.GetCountryResponse;

@SpringBootApplication
public class ConsumerApplication {

	public static void main(String[] args) {

		new LoginController();

		SpringApplication.run(ConsumerApplication.class, args);
	}

//	@Bean
//	CommandLineRunner lookup(ConsumerClient quoteClient) {
//		return args -> {
//			String country = "Spain";
//			String username = "doc";
//			String password = "doc";
//
//			if (args.length > 0) {
//				country = args[0];
//			}
//			GetCountryResponse response = quoteClient.getCountry(country);
//			System.err.println(response.getCountry().getCurrency());
//
//			GetLoginResponse userResponse = quoteClient.login(username, password);
//			// System.err.println("role " + userResponse.getUser().getRole());
//
//		};
//	}

}