package com.example.consumer.views;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.ui.ApplicationFrame;

import java.util.Map;

public class PieChart_AWT extends ApplicationFrame {

    public PieChart_AWT( String title, Map<String, Long> data ) {
        super( "Daily activity" );
        setContentPane(createDemoPanel(title, data ));
    }

    private static PieDataset createDataset( Map<String, Long> data ) {
        DefaultPieDataset dataset = new DefaultPieDataset( );

        for(String label: data.keySet()){
            dataset.setValue(label, data.get(label));
        }

        return dataset;
    }

    private static JFreeChart createChart( String title, PieDataset dataset ) {
        JFreeChart chart = ChartFactory.createPieChart(
                title,   // chart title
                dataset,          // data
                true,             // include legend
                true,
                false);

        return chart;
    }

    public static JPanel createDemoPanel(String title, Map<String, Long> data) {
        JFreeChart chart = createChart(title, createDataset( data ) );
        return new ChartPanel( chart );
    }
}