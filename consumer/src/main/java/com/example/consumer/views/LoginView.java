package com.example.consumer.views;

import lombok.Getter;
import lombok.Setter;

import javax.swing.*;

@Getter
@Setter
public class LoginView extends JFrame {
    private JPanel mainPanel;
    private JLabel usernameLabel;
    private JTextField usernameText;
    private JLabel passwordLabel;
    private JPasswordField passwordText;
    private JButton buttonLogin;

    public LoginView() {

        this.setBounds(100, 100, 500, 300);
        setTitle("Login");
        setLayout(null);

         mainPanel = new JPanel();
         mainPanel.setLayout(null);
         mainPanel.setSize( 400, 300);

         usernameLabel = new JLabel("Username");
         usernameLabel.setBounds(168, 53, 60, 14);

         usernameText = new JTextField();
         usernameText.setBounds(126, 78, 131, 20);
         usernameText.setColumns(10);

         passwordLabel = new JLabel("Password");
         passwordLabel.setBounds(168, 109, 60, 14);

         passwordText = new JPasswordField();
         passwordText.setBounds(126, 134, 131, 20);
         passwordText.setColumns(10);

         buttonLogin = new JButton("Login");
         buttonLogin.setBounds(149, 187, 89, 23);

        mainPanel.add(usernameLabel);
        mainPanel.add(usernameText);
        mainPanel.add(passwordLabel);
        mainPanel.add(passwordText);
        mainPanel.add(buttonLogin);

        this.add(mainPanel);
        this.mainPanel.setVisible(true);
        this.setVisible(true);

    }
}
