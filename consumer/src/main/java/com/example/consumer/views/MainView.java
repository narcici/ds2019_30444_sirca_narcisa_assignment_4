package com.example.consumer.views;

import com.example.consumer.ConsumerClient;
import com.example.consumer.ConsumerConfiguration;
import com.example.consumer.wsdl.ActivityXML;
import com.example.consumer.wsdl.GetLoginResponse;
import com.example.consumer.wsdl.SaveActivityRequest;
import com.example.consumer.wsdl.SaveActivityResponse;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.lang.reflect.Field;
import java.util.List;

@Setter
@Getter
public class MainView extends JFrame {

    JLabel patientName;
    JPanel mainPanel;
    JPanel patientsPanel;
    JTabbedPane tabbedPane;
    //different panels for tabbed pane
    JPanel daysHistoryPanel;
    JPanel takenMedicationPanel;
    JPanel activitiesPanel;
    JPanel medicationPlansPanel;

    // tabel + scrollpanel activites
    private JTable activitiesTable;
    private JScrollPane activitiesScrollPane;// = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

    // tabel + scrollpanel taken medications
    private JTable takenMedicationsTable;
    private JScrollPane takenMedicationsScrollPane;// = new JScrollPane(table, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);


    private ConsumerClient consumerClient;
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfiguration.class);

    public  MainView() {

        this.consumerClient = context.getBean(ConsumerClient.class);

        setLayout(null);
        setBounds(100,20,1000,800);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setTitle("Doctor");

        mainPanel = new JPanel();
        mainPanel.setBounds(0,0,1000,800);
        mainPanel.setLayout(null);
        mainPanel.setBackground(Color.white);
        this.add(mainPanel);

        patientName = new JLabel("Patient");
        patientName.setBounds(20, 10, 180, 30);
        patientName.setFont(new Font("Courier New", Font.ITALIC, 20));
        mainPanel.add(patientName);

        patientsPanel = new JPanel();
        patientsPanel.setBounds(10,100,280,700);
        patientsPanel.setLayout(null);
        patientsPanel.setBackground(Color.GRAY);
        patientsPanel.setVisible(true);

        mainPanel.add(patientsPanel);

        tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setBounds(300, 0,680,800);

        mainPanel.add(tabbedPane);
        tabbedPane.setVisible(true);

        daysHistoryPanel = new JPanel();
        daysHistoryPanel.setLayout(null);
        tabbedPane.addTab("Daily History", daysHistoryPanel);

        takenMedicationPanel = new JPanel();
        takenMedicationPanel.setLayout(null);
        tabbedPane.addTab("Taken Medicines", takenMedicationPanel);

        activitiesPanel = new JPanel();
        activitiesPanel.setLayout(null);
        tabbedPane.addTab("All Activities", activitiesPanel);

//        medicationPlansPanel =new JPanel();
//        medicationPlansPanel.setLayout(null);
//        tabbedPane.addTab("Medication Plans", medicationPlansPanel);

        mainPanel.setVisible(true);
        this.setVisible(true);
    }

    public void setPatientName(String name){
        this.patientName.setText(name);
    }

    // as assig5, PT labs
    public static String[][] createStringMatrix(List listOfObjects) {

        String[][] matrix= new String[listOfObjects.size()][listOfObjects.get(0).getClass().getDeclaredFields().length];
        int i=0;
        int j=0;
        for(Object object:listOfObjects) {
            j=0;
            for (Field field : object.getClass().getDeclaredFields()) {

                field.setAccessible(true);
                try {
                    matrix[i][j] = String.valueOf(field.get(object));

                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                j++;
            }
            i++;
        }
        return matrix;
    }

    public static String[] makeTableHeader(Object object) {
        String[] header= new String[object.getClass().getDeclaredFields().length];
        int i=0;
        for(Field s:object.getClass().getDeclaredFields()) {
            header[i]= s.getName();
            i++;
        }
        return header;
    }

    public void setActivityPanel(List listOfObjects){
        // as assig5, PT labs
        String[] header = makeTableHeader(listOfObjects.get(0));
        String[][] content = createStringMatrix(listOfObjects);
        this.activitiesTable = new JTable(content, header);
         this.activitiesScrollPane = new JScrollPane(this.activitiesTable,
                 ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                 ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.activitiesScrollPane.setBounds(10,10,660,600);
         this.activitiesScrollPane.setVisible(true);
         this.activitiesPanel.add(this.activitiesScrollPane);

         activitiesTable.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }

            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getID() == KeyEvent.KEY_PRESSED) {
                    if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                        int i = activitiesTable.getSelectedRow();
                        TableModel model = activitiesTable.getModel();

                        ActivityXML activityXml = new ActivityXML();
                        activityXml.setPatientId(Integer.valueOf(model.getValueAt(i,0).toString()));
                        activityXml.setActivityId(Integer.valueOf(model.getValueAt(i,1).toString()));
                        activityXml.setActivity(model.getValueAt(i,2).toString());
                        activityXml.setStart(model.getValueAt(i,3).toString());
                        activityXml.setEnd(model.getValueAt(i,4).toString());
                        activityXml.setRecommendation(model.getValueAt(i,5).toString());

                        SaveActivityResponse response= consumerClient.saveActivityResponse(activityXml);
                        System.out.println(response.getServerConfirmation());
                    }
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
    }

    public void setTakenMedicationsPanel(List listOfObjects){

        String[] header = makeTableHeader(listOfObjects.get(0));
        String[][] content = createStringMatrix(listOfObjects);
        this.takenMedicationsTable = new JTable(content, header);
        this.takenMedicationsScrollPane = new JScrollPane(this.takenMedicationsTable,
                ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        this.takenMedicationsScrollPane.setBounds(10,10,660,600);
        this.takenMedicationsScrollPane.setVisible(true);
        this.takenMedicationPanel.add(this.takenMedicationsScrollPane);

    }
}
