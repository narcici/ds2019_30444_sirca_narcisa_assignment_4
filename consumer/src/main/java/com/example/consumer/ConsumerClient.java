package com.example.consumer;

import com.example.consumer.wsdl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

public class ConsumerClient extends WebServiceGatewaySupport {

    private static final Logger log = LoggerFactory.getLogger(ConsumerClient.class);

    public GetCountryResponse getCountry(String country) {

        GetCountryRequest request = new GetCountryRequest();
        request.setName(country);

        log.info("Requesting location for " + country);

        GetCountryResponse response = (GetCountryResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8090/ws/countries", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetCountryRequest"));

        return response;
    }

    public GetLoginResponse login(String username, String password) {

        GetLoginRequest request = new GetLoginRequest();
        request.setUsername(username);
        request.setPassword(password);

        log.info("Requesting user for " + username + " " + password);

        GetLoginResponse response = (GetLoginResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8090/ws/soap", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetLoginRequest"));
        return response;
    }

    public GetActivitiesResponse getActivityResponse(int patientId) {

        GetActivitiesRequest request = new GetActivitiesRequest();
        request.setPatientId(patientId);

        log.info("Requesting activities for patientId: " + patientId);

        GetActivitiesResponse response = (GetActivitiesResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8090/ws/soap", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetActivitiesRequest"));
        return response;
    }

    public GetTakenMedicationsResponse getTakenMedicationsResponse(int patientId) {

        GetTakenMedicationsRequest request = new GetTakenMedicationsRequest();
        request.setPatientId(patientId);

        log.info("Requesting taken medicines for patientId: " + patientId);

        GetTakenMedicationsResponse response = (GetTakenMedicationsResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8090/ws/soap", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetTakenMedicationsRequest"));
        return response;
    }

    public GetMedicationPlansResponse getMedicationPlansResponse(int patientId) {

        GetMedicationPlansRequest request = new GetMedicationPlansRequest();
        request.setPatientId(patientId);

        log.info("Requesting medication plans for patientId: " + patientId);

        GetMedicationPlansResponse response = (GetMedicationPlansResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8090/ws/soap", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetMedicationPlansRequest"));
        return response;
    }

    public SaveActivityResponse saveActivityResponse(ActivityXML activityXML) {

        SaveActivityRequest request = new SaveActivityRequest();
        request.setActivity(activityXML);

        log.info("Requesting to save recommandation  for activity: " + activityXML.getRecommendation());

        SaveActivityResponse response = (SaveActivityResponse) getWebServiceTemplate()
                .marshalSendAndReceive("http://localhost:8090/ws/soap", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/SaveActivityRequest"));
        return response;
    }

}