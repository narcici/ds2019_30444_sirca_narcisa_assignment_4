package com.example.consumer.controllers;

import com.example.consumer.ConsumerClient;
import com.example.consumer.ConsumerConfiguration;
import com.example.consumer.views.LoginView;
import com.example.consumer.views.MainView;
import com.example.consumer.views.PieChart_AWT;
import com.example.consumer.wsdl.*;
import lombok.Getter;
import lombok.Setter;
import org.jfree.ui.RefineryUtilities;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.swing.*;
import java.awt.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class LoginController {

    private LoginView loginView;
    private  MainView mainView;
    private ConsumerClient consumerClient;
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ConsumerConfiguration.class);

    // map pt fiecare zi un alt map cu ce activitate cat a durat
    Map<String, Map<String, Long>> activitiesDayMap = new HashMap<>();

    DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");

    public LoginController() {

        this.consumerClient = context.getBean(ConsumerClient.class);
        this.loginView = new LoginView();

        this.loginView.getButtonLogin().addActionListener(e -> this.loginAction());

    }

    public void loginAction(){

        String username = this.loginView.getUsernameText().getText();
        String password = this.loginView.getPasswordText().getText();

        GetLoginResponse patientsResponse = consumerClient.login(username, password);
        if(patientsResponse != null){
            for(PatientXML p:patientsResponse.getPatientXML()){
                System.out.println(p.getName()); // works ok
            }

            mainView = new MainView();
                if(!patientsResponse.getPatientXML().isEmpty())  {
                    int x = 30;
                    for(PatientXML p: patientsResponse.getPatientXML()){
                        JButton button = new JButton(p.getName());
                        button.setBounds(20,x, 150, 30);
                        button.setBackground(Color.lightGray);
                        x += 50;
                        button.addActionListener(e -> patientButtonActionListener(p));
                        mainView.getPatientsPanel().add(button);
                    }
                }
                else System.out.println("patientsResponse nu are patienti"); // nu s a printat
        }
    }

    public void patientButtonActionListener(PatientXML patientXML)  {
        System.out.println("apasat " + patientXML.getName());

        mainView.setPatientName(patientXML.getName());

        //caut activities pt el -> activities list
        GetActivitiesResponse activitiesResponse = consumerClient.getActivityResponse(patientXML.getPatientId());
        List<ActivityXML> activityXMLList = activitiesResponse.getActivities();
        if(!activityXMLList.isEmpty())
            mainView.setActivityPanel(activityXMLList);

        initActivityDailyMap(activityXMLList);
        addDailyButtons();

        // caut taken medications pt el -> TakenMedication list
        GetTakenMedicationsResponse takenMedicationsResponse = consumerClient.getTakenMedicationsResponse(patientXML.getPatientId());
        if(!takenMedicationsResponse.getTakenMedications().isEmpty()){
            for(TakenMedicineXML a: takenMedicationsResponse.getTakenMedications()){
                System.out.println(a.getMedicine());
            }
            String[] header = mainView.makeTableHeader(takenMedicationsResponse.getTakenMedications().get(0));
            String[][] content = mainView.createStringMatrix(takenMedicationsResponse.getTakenMedications());

            mainView.setTakenMedicationsPanel(takenMedicationsResponse.getTakenMedications());
        }

        // caut medication plans  pt el -> MedicationPlan list
//        GetMedicationPlansResponse medicationPlansResponse = consumerClient.getMedicationPlansResponse(patientXML.getPatientId());
//        if(!medicationPlansResponse.getMedicationPlans().isEmpty()){
//            for(MedicationPlanXML a: medicationPlansResponse.getMedicationPlans()){
//                System.out.println(a.getIntakeIntervalsAll());
//            }
//        }

    }

    private void initActivityDailyMap( List<ActivityXML> activityXMLList){

        DateFormat formatter = new SimpleDateFormat("MMM dd yyyy");

        if(!activityXMLList.isEmpty()){
            activitiesDayMap = new HashMap<>();
            // creating a map for each day
            for(ActivityXML a: activityXMLList){
                System.out.println(a.getActivity());
                try {
                    activitiesDayMap.put(formatter.format(dateFormat.parse(a.getStart())), new HashMap<String, Long>());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            for(ActivityXML a: activityXMLList){
                System.out.println(a.getActivity());
                try {
                    Date start = dateFormat.parse(a.getStart());
                    Date end = dateFormat.parse(a.getEnd());
                    System.out.println(start + " " + end);

                    long s = start.getTime();
                    long e = end.getTime();
                    long time = e - s;
                    activitiesDayMap.get(formatter.format(dateFormat.parse(a.getStart()))).computeIfPresent(a.getActivity(), (k,v) -> v = v + time);
                    activitiesDayMap.get(formatter.format(dateFormat.parse(a.getStart()))).putIfAbsent(a.getActivity(),time);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void addDailyButtons() {
        this.mainView.getDaysHistoryPanel().removeAll();

        int x = 10;
        if(!activitiesDayMap.isEmpty()){
            for (Map.Entry<String, Map<String, Long>> entry: activitiesDayMap.entrySet()){
                JButton button = new JButton(entry.getKey());
                button.setBounds(0, x, 150, 20);
                x += 30;
                button.addActionListener(e -> showDailyChart(entry.getKey(), entry.getValue()));
                this.mainView.getDaysHistoryPanel().add(button);
            }
        }
        this.mainView.getDaysHistoryPanel().revalidate();
        this.mainView.getDaysHistoryPanel().repaint();
    }

    public void showDailyChart(String title, Map<String, Long> data){
        PieChart_AWT demo = new PieChart_AWT( title, data);
        demo.setSize( 560 , 367 );
        RefineryUtilities.centerFrameOnScreen( demo );
        demo.setVisible( true );
    }
}

