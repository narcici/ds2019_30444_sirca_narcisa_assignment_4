package com.example.pillbox;

import com.example.pillbox.controller.PillboxController;
import com.example.pillbox.entity.MedicationClient;
import com.example.pillbox.view.PillboxView;
import grpc.pillbox.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import javax.swing.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class GrpcClient {
    public static void main(String[] args) {
        PillboxView view = new PillboxView();
        PillboxController controller = new PillboxController(view);

        //test
//        controller.addMedication(new MedicationClient("test", new Date(), new Date()));

        List<String> al = new ArrayList<>();
        Date today = new Date();

        // only after 6 AM the medication plans will be requested from the client
        //if (today.getHours() > 6) {

            /// start listening on port 6565
            ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565)
                    .usePlaintext()
                    .build();

            MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub stub
                    = MedicationPlanServiceGrpc.newBlockingStub(channel);

            MedicationPlanListResponse response = stub.getMedicationPlans(MedicationPlanListRequest.newBuilder()
                    .setRequestMsg("getMedicationPlans")
                    .setPatientId(1)
                    .build());

            //interpreting the response
            for (MedicationPlanResponse m : response.getMedicationPlanListList()) {
                //print info, but we care mostly about the las token
                System.out.println(m.getMedicationPlanId() + " " + m.getStartDate() + " " + m.getEndDate() + " " + m.getIntakeIntervalsAll());
                String[] tokens = m.getIntakeIntervalsAll().split(";");
                al.addAll(new ArrayList<String>(Arrays.asList(tokens)));
            }
            channel.shutdown();

            //parsing the intake intervals input string(s)
            if (!al.isEmpty()) {
                for (String s : al) {
                    String[] meds = s.split(" ");
                    String medName = meds[0];
                    String[] times = meds[1].split("-");
                    // start of the intake interval
                    String[] benginHour = times[0].split(":");
                    Date startHour = Date.from(LocalDateTime.now()
                            .withHour(Integer.parseInt(benginHour[0]))
                            .withMinute(Integer.parseInt(benginHour[1]))
                            .atZone(ZoneId.systemDefault()).toInstant());

                    //  end of the intake interval
                    String[] finishHour = times[1].split(":");
                    Date endHour = Date.from(LocalDateTime.now()
                            .withHour(Integer.parseInt(finishHour[0]))
                            .withMinute(Integer.parseInt(finishHour[1]))
                            .atZone(ZoneId.systemDefault()).toInstant());

                    System.out.println( "amajuns sa am cv in astaa");
                    //test daca ora curenta ii intre cele 2 ore de medicamente
                   // if((today.compareTo(startHour) > 0) && (today.compareTo(endHour) < 0))
                    controller.addMedication(new MedicationClient(medName, startHour, endHour));
                }
            }

        // setting the swing "table" of medications here
            controller.addMedicationPanels();
            controller.getPillboxView().revalidate();
            controller.getPillboxView().repaint();

            Thread notTaken = new Thread() {
                @Override
                public void run() {
                    {
                        try {
                            do {
                                Date dd = new Date();
                                SimpleDateFormat formatter = new SimpleDateFormat();

                                if((dd.getMinutes() != controller.getPillboxView().getStartingMinute())
                                        && controller.getPillboxView().getNrOfMedicationsToTake() > 0) { // nu mai e acelasi minut... a trecut timpul

                                    /// start listening on port 6565
                                    ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 6565)
                                            .usePlaintext()
                                            .build();

                                    MedicationPlanServiceGrpc.MedicationPlanServiceBlockingStub stub
                                            = MedicationPlanServiceGrpc.newBlockingStub(channel);

                                    TakenMedicineResponse response = stub.isMedicationTaken(TakenMedicineRequest.newBuilder()
                                            .setIsTaken("NOT TAKEN !")
                                            .setMedication("T Medicine")
                                            .setTakenDate(formatter.format(dd))
                                            .setPatientId(1)
                                            .build());

                                    System.out.println("thread " + response.getServerConfirmation());
                                    channel.shutdown();

                                    JOptionPane.showMessageDialog(null, " You didn't take all your medicines!!");
                                }
                                Thread.sleep(30000);  // 1000 = 1 second => 30 sec
                            }while (this.isAlive());
                        }
                        catch (Exception e) {
                            System.out.println("catch exception" + e.getClass());
                        }
                    }
                }

            };

            notTaken.start();
        //}
    }
}

