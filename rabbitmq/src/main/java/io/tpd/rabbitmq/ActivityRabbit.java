package io.tpd.rabbitmq;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public final class ActivityRabbit implements Serializable {

    private final int id;
    private final String activity;
    private final Date start;
    private final Date end;
    private String recommendation;


    public ActivityRabbit(@JsonProperty("id") int id,
                          @JsonProperty("activity") String activity,
                          @JsonProperty("start") Date start,
                          @JsonProperty("end") Date end,
                          @JsonProperty("recommendation") String recommendation
                         ) {
        this.id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
        this.recommendation = recommendation;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id='" + id + "'" +
                ", activity=" + activity +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}