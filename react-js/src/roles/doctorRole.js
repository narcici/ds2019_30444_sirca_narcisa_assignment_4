import React from 'react'

import {Card, Col, NavLink, Row} from 'reactstrap';

class DoctorRole extends React.Component {

    constructor(props) {
        super(props);

        this.state = {};

    }


    componentDidMount() {

    }

    render() {
        return (
            <div>
                <Row>
                    <Col>
                        <Card body>

                            <NavLink href="/patient">Edit Patients</NavLink>
                        </Card>
                    </Col>
                </Row>

                <Row>
                    <Col>
                        <Card body>
                            <NavLink href="/caregiver">Edit Caregivers</NavLink>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card body>
                            <NavLink href="/medication">Edit Medication</NavLink>
                        </Card>
                    </Col>
                </Row>
            </div>

        );
    };

}

export default DoctorRole;
