import React from 'react'
import * as API_PATIENT from "../patient-data/patient/api/patient-api";

import {Card, Col, Row} from 'reactstrap';
import Table from "../commons/tables/table";

class PatientRole extends React.Component {

    constructor(props) {
        super(props);

        this.toggleForm = this.toggleForm.bind(this);

        this.state = {
            currentPatient: {}
        };

        this.columns = [
            {
                Header: 'Medication Plan Id',
                accessor: 'medicationPlanId',
            },
            {
                Header: "Start Date",
                accessor: "startdate",
            },
            {
                Header: "End date",
                accessor: "enddate",
            }

        ];

        // this.fetchThisPatient = this.fetchThisPatient.bind(this);
    }

    toggleForm() {
        this.setState({collapseForm: !this.state.collapseForm});
    }

    componentDidMount() {
        this.fetchThisPatient();
    }

    fetchThisPatient = () => {
        let userid = localStorage.getItem("currentUserId"); //returneaza string

        console.log("passed usr id " + parseInt(userid));
        API_PATIENT.getPatientWithUserid(parseInt(userid), (result, status, error) => {
            console.log(result);
            if (result !== null && (status === 200 || status === 201)) {
                this.setState({currentPatient: result}, function () {
                    console.log("patient is set");

                });

            } else {
                console.log("caregiver role error");
            }
            this.forceUpdate();

        });

    }

    refresh() {
        this.forceUpdate()
    }

    render() {

        return (

            <div>
                <Row>
                    <Col>
                        <Card body>
                            <h3>Patient</h3>
                            <p></p>
                            <h4>Name: {this.state.currentPatient.name}</h4>
                            <p></p>
                            <h5>Address: {this.state.currentPatient.address }</h5>
                            <h5>Birth date: {this.state.currentPatient.birthdate}</h5>
                            <h5>Gender: {this.state.currentPatient.gender}</h5>
                            <h5>Medical Record: {this.state.currentPatient.medicalrecord}</h5>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <Card body>
                            <Table
                                data={this.state.currentPatient.medicationPlanList }
                                columns={this.columns}
                                pageSize={10}
                            />
                        </Card>
                    </Col>
                </Row>
            </div>
        );

    };

}

export default PatientRole;