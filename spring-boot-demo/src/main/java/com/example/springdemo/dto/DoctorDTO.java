package com.example.springdemo.dto;

import com.example.springdemo.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DoctorDTO {

    private User user;
    private Integer doctorId;
    private String name;

}
