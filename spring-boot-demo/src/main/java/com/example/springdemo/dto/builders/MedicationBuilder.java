package com.example.springdemo.dto.builders;

import com.example.springdemo.dto.MedicationDTO;
import com.example.springdemo.entities.Medication;

public class MedicationBuilder {

    private MedicationBuilder() {}

    public static MedicationDTO generateDTOFromEntity(Medication medication) {
        return new MedicationDTO(
                medication.getId(),
                medication.getName(),
                medication.getDosage(),
                medication.getSideeffects());
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO){
        return new Medication(
                medicationDTO.getMedication_id(),
                medicationDTO.getName(),
                medicationDTO.getDosage(),
                medicationDTO.getSideeffects());
    }
}
