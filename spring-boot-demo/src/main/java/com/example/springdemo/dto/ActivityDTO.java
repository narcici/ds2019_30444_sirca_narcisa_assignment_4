package com.example.springdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityDTO {


    private Integer activityId;
    private Integer id;
    private String activity;
    private  Date start;
    private Date end;
    private String recommendation;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActivityDTO activityDTO = (ActivityDTO) o;
        return Objects.equals(activityId, activityDTO.activityId) &&
                Objects.equals(id, activityDTO.id) &&
                Objects.equals(activity, activityDTO.activity) &&
                Objects.equals(start, activityDTO.start) &&
                Objects.equals(end, activityDTO.end);
    }

    @Override
    public int hashCode() {
        return Objects.hash(activityId, id, activity, start, end, recommendation);
    }
}
