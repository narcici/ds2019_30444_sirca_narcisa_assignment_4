package com.example.springdemo.dto;
/*
not used, would have been great for assig 1
 */

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IntakeIntervalWithMedicationDTO {

    private Integer intakeIntervalId;
    private Byte morning;
    private Byte noon;
    private Byte evening;
    private MedicationDTO medication;
}