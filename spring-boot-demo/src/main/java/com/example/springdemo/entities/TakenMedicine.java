package com.example.springdemo.entities;

import lombok.Getter;

import javax.persistence.*;

import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Entity
@Table(name = "taken_medicines")
public class TakenMedicine {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "taken_medicine_id", unique = true, nullable = false)
    private Integer takenMedicineId;

    @Column(name = "patient_id")
    private  int patientId; // of the patient

    @Column(name = "medicine", length = 30)
    private  String medicine;

    @Column(name ="taken")
    private String isTaken;

    @Column(name = "taken_date")
    private Date takenDate;

    public TakenMedicine() {

    }

    public TakenMedicine(int patientId, String medicine, String isTaken, Date takenDate){
        this.patientId = patientId;
        this.medicine = medicine;
        this.isTaken = isTaken;
        this.takenDate = takenDate;
    }

    @Override
    public String toString() {
        return "TakenMedicine{" +
                "patientId='" + patientId + "'" +
                ", medicine=" + medicine +
                ", isTaken=" + isTaken +
                ", in date="+ takenDate +
                '}';
    }
}