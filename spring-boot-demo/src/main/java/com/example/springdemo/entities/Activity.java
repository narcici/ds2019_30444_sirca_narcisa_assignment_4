package com.example.springdemo.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.GenerationType.IDENTITY;

@Setter
@Getter
@Entity
@Table(name = "activity")
public final class Activity implements Serializable {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "activity_id", unique = true, nullable = false)
    private Integer activityId;

    @Column(name = "patient_id")
    private final int id; // of the patient

    @Column(name = "activity", length = 30)
    private final String activity;

    @Column(name = "start")
    private final Date start;

    @Column(name = "end")
    private final Date end;

    @Column(name = "recommendation")
    private String recommendation;


    public Activity(@JsonProperty("id") int id,
                    @JsonProperty("activity") String activity,
                    @JsonProperty("start") Date start,
                    @JsonProperty("end") Date end,
                    @JsonProperty("recommendation") String recommendation
    ) {
        this.id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
        this.recommendation = recommendation;
    }

    public Activity(Integer activityId, int id,String activity,Date start,Date end,String recommendation) {
        this.activityId = activityId;
        this.id = id;
        this.activity = activity;
        this.start = start;
        this.end = end;
        this.recommendation = recommendation;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "id='" + id + "'" +
                ", activity=" + activity +
                ", start=" + start +
                ", end=" + end +
                '}';
    }
}