package com.example.springdemo.entities;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;


@Entity
@Table(name = "patient")
public class Patient {

    @OneToOne
    @JoinColumn(name="userid")
    private User user;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "patient_id", unique = true, nullable = false)
    private Integer patientId;

    @Column(name = "name", length = 100)
    private String name;

    @Column(name="birthdate", length=45)
    private String birthdate;

    @Column(name="gender", length=10)
    private Character gender; // F M

    @Column(name="address", length=100)
    private String address;

    @Column(name="medicalrecord", length=300)
    private String medicalrecord;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "caregiver_id")
    private Caregiver caregiver;

    @ManyToOne( fetch = FetchType.LAZY)
    @JoinColumn(name = "doctor_id")
    private Doctor doctor;

    @OneToMany(mappedBy = "patient", fetch = FetchType.LAZY)
    private List<MedicationPlan> medicationPlanList = new ArrayList<MedicationPlan>();

    public Patient() {
    }

    public Patient(User user, Integer patientId, String name, String birthdate, Character gender, String address, String medicalrecord){
        this.user = user;
        this.patientId= patientId;
        this.name = name;
        this.birthdate = birthdate;
        this.gender = gender;
        this.address = address;
        this.medicalrecord = medicalrecord;
    }

    public User getUser() {  return user; }

    public void setUser(User user) {   this.user = user; }

    public Integer getPatientid() {
        return patientId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthdate() { return birthdate; }

    public Character getGender() {    return gender; }

    public String getAddress() { return address; }

    public String getMedicalrecord() { return medicalrecord; }

    public Caregiver getCaregiver() { return caregiver; }

    public void setCaregiver(Caregiver caregiver) { this.caregiver = caregiver; }

    public Doctor getDoctor() {   return doctor;  }

    public void setDoctor(Doctor doctor) {  this.doctor = doctor; }

    public List<MedicationPlan> getMedicationPlanList() {
        return medicationPlanList;
    }

    public void setMedicationPlanList(List<MedicationPlan> medicationPlanList) {
        this.medicationPlanList = medicationPlanList;
    }
}
