package com.example.springdemo.controller;

import com.example.springdemo.dto.CaregiverDTO;
import com.example.springdemo.dto.CaregiverWithPatientsDTO;
import com.example.springdemo.entities.Caregiver;
import com.example.springdemo.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")    // 'person' ii url de referinta aici
public class CaregiverController {

    private final CaregiverService caregiverService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService) {
        this.caregiverService = caregiverService;
    }

    @GetMapping()   //un ex de get
    public List<CaregiverDTO> findAllCaregivers(){
        return caregiverService.findAll();
    }

    @PostMapping()
    public Integer insertCaregiverDTO(@RequestBody CaregiverDTO caregiverDTO){
        return caregiverService.insert(caregiverDTO);
    }

    @GetMapping(value = "/{id}")
    public CaregiverDTO findCaregiverById(@PathVariable("id") Integer id){
        return caregiverService.findCaregiverById(id);
    }

   @GetMapping(value = "/byuserid/{id}")
   public CaregiverWithPatientsDTO findCaregiverByUserid(@PathVariable("id") Integer id){
        return caregiverService.findCaregiverByUserid(id);
    }

    @PutMapping()
    public Integer updateCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.update(caregiverDTO);
    }

    @DeleteMapping()
    public Integer deleteCaregiver(@RequestBody CaregiverDTO caregiverDTO) {
        return caregiverService.delete(caregiverDTO);
    }
}
