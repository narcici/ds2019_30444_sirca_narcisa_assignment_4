package com.example.producer.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name="intake_interval")
public class IntakeInterval {

    @Id
    private Integer intakeIntervalId;

    @ManyToOne(cascade= CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "medication_planId")
    private MedicationPlan medicationPlan;

    @Column(name = "morning")
    private Byte morning;

    @Column(name = "noon")
    private Byte noon;

    @Column(name = "evening")
    private Byte evening;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "medication_id")
    private Medication medication;

    public IntakeInterval() { }

    public IntakeInterval(Integer intakeIntervalId, MedicationPlan medicationPlan, Medication medication) {
        this.intakeIntervalId = intakeIntervalId;
        this.medicationPlan = medicationPlan;
        this.medication = medication;
    }
}
