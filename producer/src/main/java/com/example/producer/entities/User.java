package com.example.producer.entities;
import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "user")
public class User { ////in exemplu ii abstracta https://blog.sebastian-daschner.com/entries/jpa_joined_subclass_strategy_example

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "username", unique = true, nullable = false, length = 100)
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private Byte role; // 0 doctor, 1 pacient, 2 caregiver

    public User() {
    }

    public User(Integer id,String username, String password, Byte role) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public Integer getId() { return id;  }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() { return username; }

    public void setUsername(String username) { this.username = username; }

    public String getPassword() { return password; }

    public void setPassword(String password) { this.password = password; }

    public Byte getRole() { return role; }

    public void setRole(Byte role) { this.role = role; }
}
