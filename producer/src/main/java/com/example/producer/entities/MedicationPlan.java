package com.example.producer.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "medication_plan")
public class MedicationPlan {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medication_planId", unique = true, nullable = false)
    private Integer medicationPlanId;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    //DB modification in order to make assig 3 work
    //another column to simplify the work
    @Column(name="intakeIntervalsAll")
    private String intakeIntervalsAll;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;

    @ManyToMany(mappedBy = "medicationPlan", fetch = FetchType.LAZY)
    private List<IntakeInterval> intakeIntervalsList = new ArrayList<IntakeInterval>();

    public MedicationPlan(Integer medicationPlanId, Date startDate, Date endDate, String intakeIntervalsAll, Patient patient) {
        this.medicationPlanId = medicationPlanId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.intakeIntervalsAll = intakeIntervalsAll;
        this.patient = patient;
    }

    public MedicationPlan(Integer medicationPlanId, Date startDate, Date endDate, String intakeIntervalsAll) {
        this.medicationPlanId = medicationPlanId;
        this.startDate = startDate;
        this.endDate = endDate;
    }
}