package com.example.producer.services;

import com.example.producer.entities.TakenMedicine;
import com.example.producer.repositories.TakenMedicineRepository;
import io.spring.guides.gs_producing_web_service.GetTakenMedicationsRequest;
import io.spring.guides.gs_producing_web_service.GetTakenMedicationsResponse;
import io.spring.guides.gs_producing_web_service.TakenMedicineXML;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
public class TakenMedicineService {

    TakenMedicineRepository takenMedicineRepository;

    public TakenMedicineService(TakenMedicineRepository takenMedicineRepository) {
        this.takenMedicineRepository = takenMedicineRepository;
    }

    public GetTakenMedicationsResponse getTakenMedicationsResponse(GetTakenMedicationsRequest request) {
        GetTakenMedicationsResponse response = new GetTakenMedicationsResponse();

        DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");

        List<TakenMedicine> takenMedicines = takenMedicineRepository.findByPatientId(request.getPatientId());

        for (TakenMedicine p: takenMedicines){

            TakenMedicineXML tXML = new TakenMedicineXML();
            tXML.setPatientId(p.getPatientId());
            tXML.setMedicine(p.getMedicine());
            tXML.setTakenMedicineId(p.getTakenMedicineId());
            tXML.setTaken(p.getIsTaken());
            tXML.setTakenDate(dateFormat.format(p.getTakenDate()));

            response.getTakenMedications().add(tXML);
        }
        return response;
    }
}
