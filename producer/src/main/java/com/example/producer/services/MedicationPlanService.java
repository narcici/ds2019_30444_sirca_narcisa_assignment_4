package com.example.producer.services;

import com.example.producer.entities.MedicationPlan;
import com.example.producer.repositories.MedicationPlanRepository;
import io.spring.guides.gs_producing_web_service.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

@Service
@AllArgsConstructor
public class MedicationPlanService {

    private MedicationPlanRepository medicationPlanRepository;

    public GetMedicationPlansResponse getMedicationPlanResponse(GetMedicationPlansRequest request) {

        GetMedicationPlansResponse response = new GetMedicationPlansResponse();

            DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");

            List<MedicationPlan> medicationPlans = medicationPlanRepository.findByPatientId(request.getPatientId());

            if(!medicationPlans.isEmpty()) {
                for (MedicationPlan a : medicationPlans) {

                    MedicationPlanXML medicationPlanXML = new MedicationPlanXML();
                    medicationPlanXML.setMedicationPlanId(a.getMedicationPlanId());
                    medicationPlanXML.setIntakeIntervalsAll(a.getIntakeIntervalsAll());
                    medicationPlanXML.setPatientId(a.getPatient().getPatientid());
                    medicationPlanXML.setStartDate(dateFormat.format(a.getStartDate()));
                    medicationPlanXML.setEndDate(dateFormat.format(a.getEndDate()));

                    response.getMedicationPlans().add(medicationPlanXML);
                }
            }
            else
                System.out.println("medication plan  array gol din repository");

            return response;
        }
}
