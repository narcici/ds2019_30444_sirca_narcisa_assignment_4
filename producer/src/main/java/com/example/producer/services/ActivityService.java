package com.example.producer.services;

import com.example.producer.entities.Activity;
import com.example.producer.repositories.ActivityRepository;
import io.spring.guides.gs_producing_web_service.*;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class ActivityService {

    private ActivityRepository activityRepository;

    public ActivityService(ActivityRepository activityRepository) {
        this.activityRepository = activityRepository;
    }

    public GetActivitiesResponse getActivitiesResponse(GetActivitiesRequest request){

    GetActivitiesResponse response = new GetActivitiesResponse();

    DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");

    List<Activity> activities = activityRepository.findAllByPatientId(request.getPatientId());

        if(!activities.isEmpty()) {
        for (Activity a : activities) {
            ActivityXML activityXML = new ActivityXML();
            activityXML.setActivity(a.getActivity());
            activityXML.setActivityId(a.getActivityId());
            activityXML.setPatientId(a.getId());
            activityXML.setStart(dateFormat.format(a.getStart()));
            activityXML.setEnd(dateFormat.format(a.getEnd()));
            activityXML.setRecommendation(a.getRecommendation());

            response.getActivities().add(activityXML);
        }
    }
        else
                System.out.println("activity array gol din repository");

        return response;
}

    public SaveActivityResponse saveActivityResponse(SaveActivityRequest request){

        SaveActivityResponse response = new SaveActivityResponse();
/*
    public Activity(Integer activityId, int id,String activity,Date start,Date end,String recommendation) {
 */
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
        ActivityXML a = request.getActivity();
        try {
            Date start = dateFormat.parse(a.getStart());
            Date end = dateFormat.parse(a.getEnd());
            Activity activity = new Activity(a.getActivityId(), a.getPatientId(), a.getActivity(), start, end, a.getRecommendation());
            activityRepository.save(activity);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        response.setServerConfirmation("Server saved activity recommendation");
        return response;
    }

}
