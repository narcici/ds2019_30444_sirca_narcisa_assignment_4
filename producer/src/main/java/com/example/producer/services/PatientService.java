package com.example.producer.services;

import com.example.producer.entities.Doctor;
import com.example.producer.entities.Patient;
import com.example.producer.entities.User;
import com.example.producer.repositories.DoctorRepository;
import com.example.producer.repositories.PatientRepository;
import com.example.producer.repositories.UserRepository;
import io.spring.guides.gs_producing_web_service.GetLoginRequest;
import io.spring.guides.gs_producing_web_service.GetLoginResponse;
import io.spring.guides.gs_producing_web_service.PatientXML;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PatientService {

    private PatientRepository patientRepository;
    private DoctorRepository doctorRepository;
    private UserRepository userRepository;

    public PatientService(PatientRepository patientRepository, DoctorRepository doctorRepository, UserRepository userRepository) {
        this.patientRepository = patientRepository;
        this.doctorRepository = doctorRepository;
        this.userRepository = userRepository;
    }

    public GetLoginResponse login(GetLoginRequest request) {

        GetLoginResponse response = new GetLoginResponse();

        User user= userRepository.findUserByUsernameAndPassword(request.getUsername(), request.getPassword());

        Doctor doctor = doctorRepository.findByUser(user);

        List<Patient> patients = patientRepository.findAllByDoctor(doctor);

        for (Patient p: patients){
            PatientXML pXML = new PatientXML();
            pXML.setName(p.getName());
            pXML.setAddress(p.getAddress());
            pXML.setBirthdate(p.getBirthdate());
            pXML.setGender(toString().valueOf(p.getGender()));
            pXML.setCaregiverId(p.getCaregiver().getCaregiverId());
            pXML.setDoctorId(p.getDoctor().getDoctorId());
            pXML.setMedicalRecord(p.getMedicalrecord());
            pXML.setPatientId(p.getPatientid());
            pXML.setUserId(p.getUser().getId());
            response.getPatientXML().add(pXML);
        }
        return response;
    }
}
