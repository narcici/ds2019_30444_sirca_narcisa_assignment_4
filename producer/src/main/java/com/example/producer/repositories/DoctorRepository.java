package com.example.producer.repositories;

import com.example.producer.entities.Doctor;
import com.example.producer.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;


@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Integer> {

    @Query(value = "SELECT d " +
            "FROM Doctor d " +
            "INNER JOIN FETCH d.patients i"
    )
    List<Doctor> getAllFetch();

    Optional<Doctor> findById(Integer id);

    Doctor findByUser(User user);
}
