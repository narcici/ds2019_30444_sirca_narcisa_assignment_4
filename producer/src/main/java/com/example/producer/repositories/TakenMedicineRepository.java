package com.example.producer.repositories;

import com.example.producer.entities.TakenMedicine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TakenMedicineRepository extends JpaRepository<TakenMedicine, Integer> {

    List<TakenMedicine> findByPatientId(int patientId);
}
