package com.example.producer.repositories;


import com.example.producer.entities.Doctor;
import com.example.producer.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Integer> {
//
//    @Query(value = "SELECT p " +
//            "FROM Patient p " +
//            "INNER JOIN FETCH p.medicationPlanList i"
//    )
//    List<Patient> getAllFetch();
//
//    @Query(value = "SELECT u " +
//            "FROM Patient u " +
//            "ORDER BY u.name")
//    List<Patient> getAllOrdered();

    List<Patient> findAllByDoctor(Doctor doctor);
}
