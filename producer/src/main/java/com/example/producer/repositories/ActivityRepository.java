package com.example.producer.repositories;

import com.example.producer.entities.Activity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Integer> {

    @Query(value = "SELECT a " +
            "FROM Activity a " +
            "where a.id= ?1" )
    List<Activity> findAllByPatientId(int id);
}
