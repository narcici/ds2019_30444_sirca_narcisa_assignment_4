package com.example.producer.repositories;

import com.example.producer.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {


    @Query(value = "SELECT u " +
            "FROM User u " +
            "WHERE u.username = ?1"
    )
    User findByUsername(String username); ///param passing

    User findUserByUsernameAndPassword(String username, String password);
}
