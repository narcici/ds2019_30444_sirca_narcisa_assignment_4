package com.example.producer.repositories;

import com.example.producer.entities.Caregiver;
import com.example.producer.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaregiverRepository extends JpaRepository<Caregiver, Integer> {

    // returneaza lista de pacienti ai lui caregiver - caregiver-id
    @Query(value = "SELECT p " +
            "FROM Caregiver p " +
            "INNER JOIN FETCH p.patients i"
    )
    List<Caregiver> getAllFetch();


    @Query(value = "SELECT p " +
            "FROM Caregiver p " +
            "inner join p.user u " +

            "INNER JOIN FETCH p.patients i "+

            "where u.id = ?1"
    )
    Caregiver getAllFetchUserid(Integer id);


    @Query(value = "SELECT p " +
            "FROM Caregiver p " +
            "INNER JOIN FETCH p.patients i"
    )
    Caregiver getCaregiverByUser(User user);

    Caregiver findByName(String name);

}
