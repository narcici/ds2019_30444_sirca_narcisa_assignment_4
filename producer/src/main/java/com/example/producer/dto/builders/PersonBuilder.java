package com.example.producer.dto.builders;

import com.example.producer.dto.PersonDTO;
import com.example.producer.entities.Person;

public class PersonBuilder {

    public PersonBuilder() {
    }

    public static PersonDTO generateDTOFromEntity(Person person){
        return new PersonDTO(
                person.getId(),
                person.getName(),
                person.getEmail(),
                person.getIban(),
                person.getAddress());
    }

    public static Person generateEntityFromDTO(PersonDTO personDTO){
        return new Person(
                personDTO.getId(),
                personDTO.getName(),
                personDTO.getEmail(),
                personDTO.getIban(),
                personDTO.getAddress());
    }
}
