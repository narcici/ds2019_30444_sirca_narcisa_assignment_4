package com.example.producer.dto.builders;

import com.example.producer.dto.CaregiverWithPatientsDTO;
import com.example.producer.dto.PatientDTO;
import com.example.producer.entities.Caregiver;
import com.example.producer.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class CaregiverWithPatientsBuilder {

        private CaregiverWithPatientsBuilder(){}

        public static CaregiverWithPatientsDTO generateDTOFromEntity(Caregiver caregiver, List<Patient> patients){
            List<PatientDTO> dtos =  patients.stream()
                    .map(PatientBuilder::generateDTOFromEntity)
                    .collect(Collectors.toList());

            return new CaregiverWithPatientsDTO(
                    caregiver.getUser(),
                    caregiver.getCaregiverId(),
                    caregiver.getName(),
                    caregiver.getBirthdate(),
                    caregiver.getAddress(),
                    caregiver.getGender(),
                    dtos);
        }

    }

