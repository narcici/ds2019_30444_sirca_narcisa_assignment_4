package com.example.producer.dto.builders;

import com.example.producer.dto.ActivityDTO;
import com.example.producer.entities.Activity;

public class ActivityBuilder {

    public ActivityBuilder() {
    }

    public static ActivityDTO generateDTOFromEntity(Activity activity){
        return new ActivityDTO(
                activity.getActivityId(),
                activity.getId(),
                activity.getActivity(),
                activity.getStart(),
                activity.getEnd(),
                activity.getRecommendation());
    }

    public static Activity generateEntityFromDTO(ActivityDTO activityDTO){
        return new Activity(
                activityDTO.getActivityId(),
                activityDTO.getId(),
                activityDTO.getActivity(),
                activityDTO.getStart(),
                activityDTO.getEnd(),
                activityDTO.getRecommendation());
    }
}
