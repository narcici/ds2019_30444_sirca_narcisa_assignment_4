package com.example.producer.dto.builders;

import com.example.producer.dto.PersonDTO;
import com.example.producer.dto.PersonViewDTO;
import com.example.producer.entities.Person;

public class PersonViewBuilder {
        public static PersonViewDTO generateDTOFromEntity(Person person){
            return new PersonViewDTO(
                    person.getId(),
                    person.getName(),
                    person.getEmail());
        }

        public static Person generateEntityFromDTO(PersonViewDTO personViewDTO){
            return new Person(
                    personViewDTO.getId(),
                    personViewDTO.getName(),
                    personViewDTO.getEmail());
        }
}
