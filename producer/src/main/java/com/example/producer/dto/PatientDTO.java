package com.example.producer.dto;

import com.example.producer.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PatientDTO {

    private Integer patientId;
    private String name;
    private String address;
    private  String birthdate;
    private Character gender;
    private  String medicalrecord;
    private User user;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO patientDTO = (PatientDTO) o;
        return Objects.equals(user, patientDTO.user) &&
                Objects.equals(patientId, patientDTO.patientId) &&
                Objects.equals(name, patientDTO.name) &&
                Objects.equals(birthdate, patientDTO.birthdate) &&
                Objects.equals(medicalrecord, patientDTO.medicalrecord) &&
                Objects.equals(gender, patientDTO.gender) &&
                Objects.equals(address, patientDTO.address);
    }

    @Override
    public int hashCode() {

        return Objects.hash(user,patientId, name, address, medicalrecord, gender, birthdate);
    }
}
