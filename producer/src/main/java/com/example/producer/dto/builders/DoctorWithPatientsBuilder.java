package com.example.producer.dto.builders;

import com.example.producer.dto.DoctorWithPatientsDTO;
import com.example.producer.dto.PatientDTO;
import com.example.producer.entities.Doctor;
import com.example.producer.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class DoctorWithPatientsBuilder {

        private DoctorWithPatientsBuilder(){}

        public static DoctorWithPatientsDTO generateDTOFromEntity(Doctor doctor, List<Patient> patients){
            List<PatientDTO> dtos =  patients.stream()
                    .map(PatientBuilder::generateDTOFromEntity)
                    .collect(Collectors.toList());

            return new DoctorWithPatientsDTO(
                    doctor.getUser(),
                    doctor.getDoctorId(),
                    doctor.getName(),
                    dtos);
        }
    }
