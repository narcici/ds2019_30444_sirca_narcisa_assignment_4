package com.example.producer.dto;

import com.example.producer.entities.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CaregiverWithPatientsDTO {
    private User user;
    private Integer id;
    private String name;
    private String address;
    private  String birthdate;
    private Character gender;
    private List<PatientDTO> patientDTOList;


}
