package com.example.producer.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MedicationDTO {

    private Integer medication_id;
    private String name;
    private String dosage;
    private String sideeffects;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO medicationDTO = (MedicationDTO) o;
        return Objects.equals(medication_id, medicationDTO.medication_id) &&
                Objects.equals(name, medicationDTO.name) &&
                Objects.equals(dosage, medicationDTO.dosage) &&
                Objects.equals(sideeffects, medicationDTO.sideeffects);
    }

    @Override
    public int hashCode() {
        return Objects.hash(medication_id, name, dosage, sideeffects);
    }
}
