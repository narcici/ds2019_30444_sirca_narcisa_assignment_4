package com.example.producer.dto.builders;

import com.example.producer.dto.MedicationDTO;
import com.example.producer.entities.Medication;

public class MedicationBuilder {

    private MedicationBuilder() {}

    public static MedicationDTO generateDTOFromEntity(Medication medication) {
        return new MedicationDTO(
                medication.getId(),
                medication.getName(),
                medication.getDosage(),
                medication.getSideeffects());
    }

    public static Medication generateEntityFromDTO(MedicationDTO medicationDTO){
        return new Medication(
                medicationDTO.getMedication_id(),
                medicationDTO.getName(),
                medicationDTO.getDosage(),
                medicationDTO.getSideeffects());
    }
}
