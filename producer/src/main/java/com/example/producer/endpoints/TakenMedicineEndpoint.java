package com.example.producer.endpoints;

import com.example.producer.services.TakenMedicineService;
import io.spring.guides.gs_producing_web_service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class TakenMedicineEndpoint {

    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    private TakenMedicineService takenMedicineService;

    @Autowired
    public TakenMedicineEndpoint(TakenMedicineService takenMedicineService) {
        this.takenMedicineService = takenMedicineService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getTakenMedicationsRequest")
    @ResponsePayload
    public GetTakenMedicationsResponse getTakenMedicationsResponse(@RequestPayload GetTakenMedicationsRequest request) {

        return takenMedicineService.getTakenMedicationsResponse(request);
    }
}