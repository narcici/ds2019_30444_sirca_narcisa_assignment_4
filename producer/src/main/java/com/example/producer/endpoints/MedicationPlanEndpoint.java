package com.example.producer.endpoints;

import com.example.producer.services.MedicationPlanService;
import io.spring.guides.gs_producing_web_service.GetMedicationPlansRequest;
import io.spring.guides.gs_producing_web_service.GetMedicationPlansResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class MedicationPlanEndpoint {

    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    private MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanEndpoint(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getMedicationPlansRequest")
    @ResponsePayload
    public GetMedicationPlansResponse getTakenMedicationsResponse(@RequestPayload GetMedicationPlansRequest request) {
        return medicationPlanService.getMedicationPlanResponse(request);
    }
}