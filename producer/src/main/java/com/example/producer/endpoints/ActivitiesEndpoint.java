package com.example.producer.endpoints;

import com.example.producer.services.ActivityService;
import io.spring.guides.gs_producing_web_service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class ActivitiesEndpoint {

    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";

    private ActivityService activityService;

    @Autowired
    public ActivitiesEndpoint(ActivityService activityService) {

        this.activityService = activityService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getActivitiesRequest")
    @ResponsePayload
    public GetActivitiesResponse getActivitiesResponse(@RequestPayload GetActivitiesRequest request) {
        return activityService.getActivitiesResponse(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveActivityRequest")
    @ResponsePayload
    public SaveActivityResponse saveActivityResponse(@RequestPayload SaveActivityRequest request) {
        return activityService.saveActivityResponse(request);
    }
}